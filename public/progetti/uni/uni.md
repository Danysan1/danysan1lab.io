# Dente di sega

<img src="svgs/1e2f5f1fcb45de5a9a818ca7d91c25a7.svg?invert_in_darkmode" align=middle width=164.67979439999996pt height=24.65753399999998pt/>

<img src="svgs/a7d321633308ce740581380986ab7683.svg?invert_in_darkmode" align=middle width=224.42966039999996pt height=30.632847300000012pt/>

<img src="svgs/ed0d9f3133e5293cd5b33250ce6b7ab3.svg?invert_in_darkmode" align=middle width=232.37871524999994pt height=50.15378279999997pt/>

[Codice](dentedisega.m), [Immagine](dentedisega.png)

# I27368

<img src="svgs/32921d2094690fc6850feb7a0e5cb342.svg?invert_in_darkmode" align=middle width=309.51971324999994pt height=47.6716218pt/>

<img src="svgs/a7d321633308ce740581380986ab7683.svg?invert_in_darkmode" align=middle width=224.42966039999996pt height=30.632847300000012pt/>

<img src="svgs/5f0b9022c59b5871f972b26d15929fd5.svg?invert_in_darkmode" align=middle width=293.58451979999995pt height=43.428557699999985pt/>

[Codice](I27368_6.m), [Immagine](I27368_6.png)

# I27461

<img src="svgs/25de1fa06ec6bc6c974a1ba7b2d60ded.svg?invert_in_darkmode" align=middle width=258.3782652pt height=47.6716218pt/>

<img src="svgs/ee88192da6185eed3317a2d4d30f67f8.svg?invert_in_darkmode" align=middle width=243.99585044999995pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/97555d8b8fc8af15dd8af09f1401b582.svg?invert_in_darkmode" align=middle width=276.50126625pt height=43.428557699999985pt/>

[Codice](I27461_6.m), [Immagine](I27461_6.png)

# I28359

<img src="svgs/9b64b46bd472abe76c54ad98d6969a76.svg?invert_in_darkmode" align=middle width=304.30276469999995pt height=47.6716218pt/>

<img src="svgs/9f26f1338874c3ccb2551f245ad99f22.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/07e619067dcf4065294ce2dfe38b2005.svg?invert_in_darkmode" align=middle width=302.68587855pt height=43.428557699999985pt/>

[Codice](I28359_6.m), [Immagine](I28359_6.png)

# I28549

<img src="svgs/95b052ed8c685a2b04bb53277d8a363c.svg?invert_in_darkmode" align=middle width=274.8166146pt height=47.6716218pt/>

<img src="svgs/9f26f1338874c3ccb2551f245ad99f22.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/3b0c9ad4a3bc518c65666b78e227ab32.svg?invert_in_darkmode" align=middle width=277.23659864999996pt height=50.15378279999997pt/>

[Codice](I28549_6.m), [Immagine](I28549_6.png)

# I28659

<img src="svgs/9b64b46bd472abe76c54ad98d6969a76.svg?invert_in_darkmode" align=middle width=304.30276469999995pt height=47.6716218pt/>

<img src="svgs/526bfbac7557255bc961e71c662fa40d.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/fb4d654c52457a7c4b8a99361de85522.svg?invert_in_darkmode" align=middle width=270.68889045pt height=50.15378279999997pt/>

[Codice](I28659_6.m), [Immagine](I28659_6.png)

# I28749

<img src="svgs/95b052ed8c685a2b04bb53277d8a363c.svg?invert_in_darkmode" align=middle width=274.8166146pt height=47.6716218pt/>

<img src="svgs/526bfbac7557255bc961e71c662fa40d.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/58c1566cf6d207e5269703f210ee587b.svg?invert_in_darkmode" align=middle width=287.97965624999995pt height=43.428557699999985pt/>

[Codice](I28749_6.m), [Immagine](I28749_6.png)

# I32596

<img src="svgs/b1661ea02bc7eec10b5063e4f4c14fe5.svg?invert_in_darkmode" align=middle width=187.57922205pt height=26.76175259999998pt/>

<img src="svgs/6321c7932f585f13c21bfa676562e67a.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/bcccf1a514a0379db83ba903eb3551c1.svg?invert_in_darkmode" align=middle width=264.44985434999995pt height=50.15378279999997pt/>

[Codice](I32596_6.m), [Immagine](I32596_6.png)

# I38259

<img src="svgs/d6c4f96a034756ffc7cb8cead3333d93.svg?invert_in_darkmode" align=middle width=304.30276469999995pt height=47.6716218pt/>

<img src="svgs/80d7ce71c1e6d36fd0f02c1a522db21e.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/d81ee109c10936f5eec18864f21185b1.svg?invert_in_darkmode" align=middle width=325.3322407499999pt height=43.428557699999985pt/>

[Codice](I38259_6.m), [Immagine](I38259_6.png)

# I38359

<img src="svgs/a49b22fdb30c50f95d5a97bbbbd9be83.svg?invert_in_darkmode" align=middle width=304.30276469999995pt height=47.6716218pt/>

<img src="svgs/69728e599725f6a10ac640659bc789a2.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/568a301c7bd61f5dc4c05f55c270cb6f.svg?invert_in_darkmode" align=middle width=292.86931245pt height=50.15378279999997pt/>

[Codice](I38359_6.m), [Immagine](I38359_6.png)

# I38659

<img src="svgs/44529690937408c3866e5ffd1e58347a.svg?invert_in_darkmode" align=middle width=304.30276469999995pt height=47.6716218pt/>

<img src="svgs/d8d3e0964eb11658df6100feba84a324.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/fe4890a0cbe9ad31244a87cd69cb1bb7.svg?invert_in_darkmode" align=middle width=325.3322407499999pt height=43.428557699999985pt/>

[Codice](I38659_6.m), [Immagine](I38659_6.png)

# I49327

<img src="svgs/e5df0fffaa0d78874c23d2ebac003d91.svg?invert_in_darkmode" align=middle width=287.60204219999997pt height=47.6716218pt/>

<img src="svgs/9f26f1338874c3ccb2551f245ad99f22.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/61d219c55d0a0d1955bf1b59c6941e06.svg?invert_in_darkmode" align=middle width=280.078392pt height=50.15378279999997pt/>

[Codice](I49327_6.m), [Immagine](I49327_6.png)

# I49372

<img src="svgs/2c86263cbed79d311a4375b5253282d7.svg?invert_in_darkmode" align=middle width=187.93308764999998pt height=26.76175259999998pt/>

<img src="svgs/3b299e53965e1f79267bac2e4533f994.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/0a850868fac9f5c764a3e1d0a9e9786a.svg?invert_in_darkmode" align=middle width=290.7842453999999pt height=43.428557699999985pt/>

[Codice](I49372_6.m), [Immagine](I49372_6.png)

# I49827

<img src="svgs/e5df0fffaa0d78874c23d2ebac003d91.svg?invert_in_darkmode" align=middle width=287.60204219999997pt height=47.6716218pt/>

<img src="svgs/526bfbac7557255bc961e71c662fa40d.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/90053f86e3bcb9b90135cd0f5fd69e7d.svg?invert_in_darkmode" align=middle width=276.03533264999993pt height=43.428557699999985pt/>

[Codice](I49827_6.m), [Immagine](I49827_6.png)

# I78123

<img src="svgs/2215ca186069adedd5bfa14101ea1b4c.svg?invert_in_darkmode" align=middle width=180.20476155pt height=24.65753399999998pt/>

<img src="svgs/c7ae2a5975d2f3bc8441c90540ae4a62.svg?invert_in_darkmode" align=middle width=249.47531894999997pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/cc6d0ed3345228e72b682c87a0e3fd9f.svg?invert_in_darkmode" align=middle width=239.42757629999997pt height=50.15378279999997pt/>

[Codice](I78123_6.m), [Immagine](I78123_6.png)

# I78523

<img src="svgs/2215ca186069adedd5bfa14101ea1b4c.svg?invert_in_darkmode" align=middle width=180.20476155pt height=24.65753399999998pt/>

<img src="svgs/9e3b26a54bc25b6f82dc7a149251f684.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/713d9a06459d811a224e56c6f1e228e4.svg?invert_in_darkmode" align=middle width=304.77094394999995pt height=43.428557699999985pt/>

[Codice](I78523_6.m), [Immagine](I78523_6.png)

# I85634

<img src="svgs/ef21c80ae35f95e95af47f41fa71acc6.svg?invert_in_darkmode" align=middle width=186.75730754999998pt height=26.76175259999998pt/>

<img src="svgs/6321c7932f585f13c21bfa676562e67a.svg?invert_in_darkmode" align=middle width=229.29263115pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/24ec7cbd340e0a8df09691cd9af30824.svg?invert_in_darkmode" align=middle width=292.6107096pt height=43.428557699999985pt/>

**ERRATO, NON VISUALIZZARE**

[Codice](I85634_6.m), [Immagine](I85634_6.png)

# T23567

<img src="svgs/473ee3aae533bdbbdba5a26052fb9117.svg?invert_in_darkmode" align=middle width=180.20476155pt height=24.65753399999998pt/>

<img src="svgs/a7d321633308ce740581380986ab7683.svg?invert_in_darkmode" align=middle width=224.42966039999996pt height=30.632847300000012pt/>

<img src="svgs/c021d7b404172e466bc4c58d6bf9f51b.svg?invert_in_darkmode" align=middle width=201.05592209999998pt height=46.8988773pt/>

[Codice](T23567_6.m), [Immagine](T23567_6.png)

# T36691

<img src="svgs/344e20b52558d083a909311b9741e836.svg?invert_in_darkmode" align=middle width=164.67979439999996pt height=24.65753399999998pt/>

<img src="svgs/39f2693891d9d77294b5f834f1c855bc.svg?invert_in_darkmode" align=middle width=223.81316264999998pt height=24.65753399999998pt/>

<img src="svgs/d8ca3cf05a6a17d707f4f967afbac9e3.svg?invert_in_darkmode" align=middle width=230.50266194999998pt height=30.267491100000004pt/>

<img src="svgs/24a94fe675d104ae83c8a9948d48bda4.svg?invert_in_darkmode" align=middle width=334.61518859999995pt height=43.428557699999985pt/>

[Codice](T36691_6.m), [Immagine](T36691_6.png)
