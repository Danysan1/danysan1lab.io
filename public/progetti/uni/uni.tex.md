# Dente di sega

$f: [-5,5] \rightarrow \mathbb{R} : x \rightarrow x$

$\underline{\tilde{f} \text{ estensione periodica di } f \text{ a } \mathbb{R}}$

$\tilde{f}(x) = \dfrac{10}{\pi} \sum\limits_{k=1}^{\infty} \dfrac{(-1)^{k+1}}{k} \sin(\frac{\pi}{5} k x)$

[Codice](dentedisega.m), [Immagine](dentedisega.png)

# I27368

$f: [0,3] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
1 \text { se } x \in [0,1] \cup [2,3] \\
0 \text { se }  x \in [1,2]
\end{array}\right.$

$\underline{\tilde{f} \text{ estensione periodica di } f \text{ a } \mathbb{R}}$

$\tilde{f}(x) = \dfrac{2}{3} + \dfrac{2}{\pi} \sum\limits_{k=1}^{\infty} \dfrac{1}{k} \cos(\frac{2}{3} \pi k x) \sin(\frac{2}{3} \pi k x)$

[Codice](I27368_6.m), [Immagine](I27368_6.png)

# I27461

$f: [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
1 \text { se } x \in [0,1] \\
0 \text { se }  x \in [1,2]
\end{array}\right.$

$F \text{ estensione dispari di } f \text{ a } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{2}{\pi} \sum\limits_{k=1}^{\infty} \dfrac{1}{k} (1 - \cos(\frac{\pi}{2} k)) \sin(\frac{\pi}{2} k x)$

[Codice](I27461_6.m), [Immagine](I27461_6.png)

# I28359

$f : [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
x & \text { se } x \in [0,1] \\
2-x & \text { se }  x \in [1,2]
\end{array}\right.$

$F \text{ estensione pari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = \dfrac{1}{2} - \dfrac{4}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{1}{(2k+1)^2} \cos((2k+1) \pi x)$

[Codice](I28359_6.m), [Immagine](I28359_6.png)

# I28549

$f : [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
0 & \text { se } x \in [0,1] \\
1 & \text { se }  x \in [1,2]
\end{array}\right.$

$F \text{ estensione pari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = \dfrac{1}{2} - \dfrac{2}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{(-1)^k}{2k+1} \cos(\frac{\pi}{2}(2k+1)x)$

[Codice](I28549_6.m), [Immagine](I28549_6.png)

# I28659

$f : [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
x & \text { se } x \in [0,1] \\
2-x & \text { se }  x \in [1,2]
\end{array}\right.$

$F \text{ estensione dispari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = \dfrac{8}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{(-1)^k}{(2k+1)^2} \sin(\frac{\pi}{2}(2k+1)x)$

[Codice](I28659_6.m), [Immagine](I28659_6.png)

# I28749

$f : [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
0 & \text { se } x \in [0,1] \\
1 & \text { se }  x \in [1,2]
\end{array}\right.$

$F \text{ estensione dispari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = \dfrac{2}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{1}{k} (\cos(\frac{\pi}{2}k)-(-1)^k) \sin(\frac{\pi}{2}kx)$

[Codice](I28749_6.m), [Immagine](I28749_6.png)

# I32596

$f: [0,1] \rightarrow \mathbb{R} : x \rightarrow x^2-1$

$F \text{ estensione pari di } f \text{ in } [-1,1]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = -\dfrac{2}{3} + \dfrac{4}{\pi^2} \sum\limits_{k=1}^{\infty} \dfrac{(-1)^k}{k^2} \cos(\pi k x)$

[Codice](I32596_6.m), [Immagine](I32596_6.png)

# I38259

$f: [0,6] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
3-x & \text { se } x \in [0,3] \\
0 & \text { se } x \in [3,6]
\end{array}\right.$

$F \text{ estensione pari di } f \text{ in } [-6,6]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{3}{4} + \dfrac{12}{\pi^2}\sum\limits_{k=1}^{\infty} \dfrac{1}{k^2} (1-\cos(\frac{\pi}{2}k))\cos(\frac{\pi}{6}kx)$

[Codice](I38259_6.m), [Immagine](I38259_6.png)

# I38359

$f: [0,6] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
x & \text { se } x \in [0,3] \\
6-x & \text { se } x \in [3,6]
\end{array}\right.$

$F \text{ estensione dispari di } f \text{ in } [-6,6]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{24}{\pi^2} \sum\limits_{k=0}^{\infty}\dfrac{(-1)^k}{(2k+1)^2} \sin(\frac{\pi}{6}(2k+1)x)$

[Codice](I38359_6.m), [Immagine](I38359_6.png)

# I38659

$f: [0,4] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
2-x & \text { se } x \in [0,2] \\
0 & \text { se } x \in [2,4]
\end{array}\right.$

$F \text{ estensione pari di } f \text{ in } [-4,4]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{1}{2} + \dfrac{8}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{1}{k^2} (1 - \cos(\frac{\pi}{2}k)) \cos(\frac{\pi}{4}kx)$

[Codice](I38659_6.m), [Immagine](I38659_6.png)

# I49327

$f: [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
-1 & \text { se } x \in [0,1] \\
1 & \text { se } x \in [1,2]
\end{array}\right.$

$F \text{ estensione pari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{4}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{(-1)^{k+1}}{2k+1} \cos(\frac{\pi}{2}(2k+1)x)$

[Codice](I49327_6.m), [Immagine](I49327_6.png)

# I49372

$f: [0,1] \rightarrow \mathbb{R} : x \rightarrow x-x^2$

$F \text{ estensione dispari di } f \text{ in } [-1,1]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{8}{\pi^3} \sum\limits_{k=0}^{\infty} \dfrac{1}{(2k+1)^3} \sin((2k+1)\pi x)$

[Codice](I49372_6.m), [Immagine](I49372_6.png)

# I49827

$f: [0,2] \rightarrow \mathbb{R} : x \rightarrow \left\{\begin{array}{ll}
-1 & \text { se } x \in [0,1] \\
1 & \text { se } x \in [1,2]
\end{array}\right.$

$F \text{ estensione dispari di } f \text{ in } [-2,2]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = -\dfrac{4}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{1}{2k+1} \sin((2k+1)\pi x)$

[Codice](I49827_6.m), [Immagine](I49827_6.png)

# I78123

$f : [0,5] \rightarrow \mathbb{R} : x \rightarrow x+1$

$F \text{ estensione dispari di } f \text{ in } [-5,5]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = - \dfrac{2}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{6(-1)^k-1}{k} \sin(\frac{\pi}{5}kx)$

[Codice](I78123_6.m), [Immagine](I78123_6.png)

# I78523

$f : [0,5] \rightarrow \mathbb{R} : x \rightarrow x+1$

$F \text{ estensione pari di } f \text{ in } [-5,5]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F} = \dfrac{7}{2} - \dfrac{20}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{1}{(2k+1)^2} \cos(\frac{\pi}{5}(2k+1)x)$

[Codice](I78523_6.m), [Immagine](I78523_6.png)

# I85634

$f: [0,1] \rightarrow \mathbb{R} : x \rightarrow 1-x^2$

$F \text{ estensione pari di } f \text{ in } [-1,1]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{F}(x) = \dfrac{8}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{1}{(2k+1)^2} \cos((2k+1)\pi x)$

**ERRATO, NON VISUALIZZARE**

[Codice](I85634_6.m), [Immagine](I85634_6.png)

# T23567

$f: [0,1] \rightarrow \mathbb{R} : x \rightarrow 1-x$

$\underline{\tilde{f} \text{ estensione periodica di } f \text{ a } \mathbb{R}}$

$\tilde{f}(x) = \dfrac{1}{2} + \dfrac{1}{\pi} \sum\limits_{k=1}^{\infty} \dfrac{\sin({2 \pi k x})}{k}$

[Codice](T23567_6.m), [Immagine](T23567_6.png)

# T36691

$f: [0,1] \rightarrow \mathbb{R} : x \rightarrow -x$

$F \text{ estensione pari di } f \text{ a } [-1,1]$

$\underline{\tilde{F} \text{ estensione periodica di } F \text{ a } \mathbb{R}}$

$\tilde{f}(x) = -\dfrac{1}{2} + \dfrac{4}{\pi^2} \sum\limits_{k=0}^{\infty} \dfrac{1}{(2k+1)^2} \cos({(2k+1)\pi x})$

[Codice](T36691_6.m), [Immagine](T36691_6.png)
