<!DOCTYPE html>
<html>
	<head>
		<title>Font family</title>
		<meta name="description" content="Valori CSS font-family" />
		<link href='https://fonts.googleapis.com/css?family=Zilla Slab Highlight' rel='stylesheet'>
	</head>
	<body>
		<?php
		$fonts = [
			'serif', 'sans-serif', 'monospace', 'cursive', 'fantasy', 'system-ui', 'emoji', 'math', 'fangsong', 'Courier',
			'Courier New', 'Franklin Gothic Medium', 'Arial Narrow', 'Arial', 'Gill Sans', 'Gill Sans MT', 'Calibri',
			'Trebuchet MS', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', 'Geneva',
			'Verdana', 'Segoe UI', 'Tahoma', 'Times New Roman', 'Times', 'Helvetica', 'Cambria', 'Cochin', 'Georgia',
			'Impact', 'Haettenschweiler', 'Arial Narrow Bold', '-apple-system', 'BlinkMacSystemFont', 'Roboto', 'Oxygen',
			'Ubuntu', 'Cantarell', 'Open Sans', 'Helvetica Neue', 'Palatino Linotype', 'Book Antiqua', 'Palatino',
			'Arial Black', 'Gadget', 'Comic Sans MS', 'Charcoal', 'Lucida Console', 'Monaco', 'Gill Sans Extrabold',
			'Goudy Bookletter 1911', 'Fira Code', 'Fira Sans', 'Fira Mono', 'Garamond', 'Bookman', 'Andale Mono',
			'Profont', 'Droid Sans Mono', 'DejaVu Sans Mono', 'Consolas', 'Inconsolata'
		];
		sort($fonts);
		foreach ($fonts as $font) { ?>
			<h1 style="font-family:'<?=$font;?>', Zilla Slab Highlight;"><?=$font;?>: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis semper.</h1>
		<?php } ?>
	</body>
</html>
