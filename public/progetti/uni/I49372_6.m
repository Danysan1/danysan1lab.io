clear;

% Configurazione
xMin=-2
xMax=2
yMin=-1
yMax=1
K=100 % Numero totale armoniche
V=4 % Numero armoniche visibili

% Codice
sum=0;
clf
x=xMin:.01:xMax;
hold on
for k=0:1:K,
    sum = sum+sin((2*k+1)*x*pi)/(2*k+1)^3;
    f=8/pi^3*sum;
    if (k < V)
      plot(x,f,'DisplayName',num2str(k))
    elseif (k == K)
      plot(x,f,'LineWidth',2,'DisplayName',num2str(K))
    end
end
hold off
legend('show')
title(mfilename)
xlabel('x')
ylabel('F(x)')
grid on
axis([xMin,xMax,yMin,yMax])

% Stampa il grafico
print([mfilename,'.png'],'-dpng','-r300')
