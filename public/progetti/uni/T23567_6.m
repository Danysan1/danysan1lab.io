clear;

% Configurazione
xMin=-1
xMax=2
yMin=-1
yMax=2
K=2000 % Numero totale armoniche
V=6 % Numero armoniche visibili

% Codice
sum=0;
clf
x=xMin:.01:xMax;
hold on
for k=1:1:K,
    sum = sum+sin(2*k*x*pi)/k;
    f=1/2+1/pi*sum;
    if (k < V)
      plot(x,f,'DisplayName',num2str(k))
    elseif (k == K)
      plot(x,f,'LineWidth',2,'DisplayName',num2str(K))
    end
end
hold off
legend('show')
title(mfilename)
xlabel('x')
ylabel('F(x)')
grid on
axis([xMin,xMax,yMin,yMax])

% Stampa il grafico
print([mfilename,'.png'],'-dpng','-r300')
