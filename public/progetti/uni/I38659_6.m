clear;

% Configurazione
xMin=-8
xMax=8
yMin=-1
yMax=3
K=100 % Numero totale armoniche
V=10 % Numero armoniche visibili

% Codice
sum=0;
clf
x=xMin:.01:xMax;
hold on
for k=1:1:K,
    sum = sum+1/k^2*(1-cos(k*pi/2))*cos(k*x*pi/4);
    f=1/2+8/pi^2*sum;
    if (k < V)
      plot(x,f,'DisplayName',num2str(k))
    elseif (k == K)
      plot(x,f,'LineWidth',2,'DisplayName',num2str(K))
    end
end
hold off
legend('show')
title(mfilename)
xlabel('x')
ylabel('F(x)')
grid on
axis([xMin,xMax,yMin,yMax])

% Stampa il grafico
print([mfilename,'.png'],'-dpng','-r300')
