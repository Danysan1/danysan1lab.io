// https://www.w3schools.com/howto/howto_css_modal_images.asp
// Get the modal
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");

// Inizializza le immagini
imgs = document.getElementsByClassName("thumb")
for (var i = 0; i < imgs.length; i++) {
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = imgs[i]
    img.onclick = function() {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    }
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
var nascondi = function() {
    modal.style.display = "none";
}
span.onclick = nascondi
modal.onclick = nascondi


// https://varvy.com/pagespeed/defer-images.html
window.onload = function() {
    var imgDefer = document.getElementsByTagName('img');
    for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
    }
}
